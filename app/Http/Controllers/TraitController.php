<?php
namespace Pad\Http\Controllers;

//use Datatables;
use Illuminate\Http\Request;
use Pad\Http\Requests;
use Pad\User;
use Redirect;
use Session;

Trait  traitController {
	
	private $pathView = [];
	private $paramsView = [];
	private $dataRepository;
	private $entityName = "model";



	public function setEntityName($entityName) {
		$this->entityName = $entityName;
	}

	public function getEntityName() {
		return $this->entityName;
	}

	public function getDataRepository() {
		return $this->dataRepository;
	}

	public function setDataRepository($dataRepository) {
		return $this->dataRepository = $dataRepository;
	}


	public function setPathViews($paths) {
		$this->pathView= $paths;
	}

	public function setPathView($key,$value) {
		$this->pathView[$key]= $value;
	}
	

	public function getPathView($key) {

		if (array_key_exists($key,$this->pathView)) {

			return $this->pathView[$key];
		}


		if (array_key_exists("BASE_PATH",$this->pathView)) {

			return $this->pathView["BASE_PATH"] . "." . $key;

		}
	}


	protected function setParamsViews($paramsView) {
		
		$this->paramsView = $paramsView;
	}

	protected function setParamView($keyView,$keyParam,$valueParam) {

		$this->paramsView[$keyView][$keyParam] = $valueParam;
	}


	protected function getParamsView($keyView) {

		if (array_key_exists($keyView,$this->paramsView)) {

			return $this->paramsView[$keyView];
		}

		return [];
	}



	protected function getParamView($keyView, $keyParam) {

		if (array_key_exists($keyView,$this->paramsView)) {

			return $this->paramsView[$keyView][$keyParam];
		}

		return false;
		
	}


	/* standard routes */


//	public function dataTables() {
//		$items = $this->getDataRepository()->all();
//	    return Datatables::of($items)->make(true);
//    }


	public function index() {

		$view = $this->getPathView("index");

		$params = $this->getParamsView('index');

		return view($view,$params);
	}


	public function create() {

		$view = $this->getPathView("create");
		
		$params = $this->getParamsView("create");

		return view($view,$params);
	}



	public function edit($model) {

		$view = $this->getPathView("edit");	

		$this->setParamView("edit",$this->getEntityName(),$model);

		$params = $this->getParamsView("edit");

		return view($view, $params);

	}


	public function show($model) {

		$view = $this->getPathView("show");		

		$this->setParamView("show",$this->getEntityName(),$model);

		$params = $this->getParamsView("show");

		return view($view, $params);
	}

	public function store(Request $request) {

      $input = $request->all();

      $errors = $this->getDataRepository()->create($input);
      
      if (!count($errors)) {

      	   $msg = $this->getParamView("create.success","message");

      	   if (!$msg) $msg = "Item Creado";

      	   Session::flash('message', $msg);

      	   return redirect($this->getPathView("create.success"));
      }

      return view($this->getPathView("create.error"))->withErrors($errors);
     
    }

	public function destroy($object)
	{
		$this->getDataRepository()->delete($object);
		$msg = $this->getParamView("delete.success","message");

		if (!$msg) $msg = "Item Eliminado";

		Session::flash('message', $msg);

		return redirect($this->getPathView("delete.success"));
	}

    public function update(Request $request, $object) {

      $input = $request->all();
            
      $errors = $this->getDataRepository()->update($input,$object);

      if (!count($errors)) {
        
        $msg = $this->getParamView("edit.success","message");

      	if (!$msg) $msg = "Item Actualizado";

      	   	Session::flash('message', $msg);

        	return redirect($this->getPathView("edit.success"));
      }
     
      return redirect()->route($this->getPathView("edit.error"), $object)
                       ->withInput()
                       ->withErrors($errors);
    }


}