<?php

namespace Pad\Http\Controllers;

use Pad\Repositories\UserRepository;
use Pad\Repositories\RoleRepository;


class UserController extends Controller
{
    
    use TraitController;

    public function __construct(UserRepository $repository) {

     
      $this->setPathViews([ "index"=>"user.index",
                            "create"=>"user.create",
                            "create.success"=>"users",
                            "create.error"=>"user.create",
                            "edit"=>"user.edit",
                            "edit.success"=>"users",
                            "edit.error"=>"user.edit",
                            "delete.success"=>"users",
                            "show"=>"user.show"
      ]);

      // Data for inputs
      $users= $repository->all();
      $roles= RoleRepository::allRoles()->pluck('name', 'id');

      $this->setParamsViews(['index'=> compact('users'),
                             'create'=> compact('roles'),
                             'edit'=> compact('roles'),
                             "create.success"=>["message"=>"user creado"],
                             "edit.success"=>["message"=>"user Actualizado"]
      ]);

      $this->setDataRepository($repository);
      $this->setEntityName("user"); // default is model
     

    }

}