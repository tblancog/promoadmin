<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');

// Users
Route::group(['prefix'=> 'users', 'middleware'=> 'auth'], function(){

    Route::get('/', ['as'=> 'user.index', 'uses'=> 'UserController@index'])->where('format', 'json|html');
    Route::get('/{user}', ['as'=> 'user.show', 'uses'=> 'UserController@show'])->where('user', '[0-9]+');

    Route::get('/create', ['as'=> 'user.create', 'uses'=> 'UserController@create']);
    Route::post('/', ['as'=> 'user.store', 'uses'=> 'UserController@store']);

    Route::get('/{user}/edit', ['as'=> 'user.edit', 'uses'=> 'UserController@edit']);
    Route::patch('/{user}', ['as'=> 'user.update', 'uses'=> 'UserController@update']);

    Route::get('/{user}/delete', ['as'=> 'user.destroy', 'uses'=> 'UserController@destroy']);
});

Route::bind('user', function($id){

    return Pad\Repositories\UserRepository::getById($id);
});