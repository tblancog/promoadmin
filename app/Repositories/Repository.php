<?php

namespace Pad\Repositories;
use Validator;


Abstract Class Repository  {

	public  function __call($name,$args) {

		return parent::$name($args);
	}


	public static function validate($data,$id=false, $rules,$msgErrors=[]) {

    	if ($id) $rules = str_replace(':id', $id, $rules);

	   	return Validator::make($data, $rules, $msgErrors);
    }

}