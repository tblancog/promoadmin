<?php
/**
 * Created by PhpStorm.
 * User: tblanco
 * Date: 02/06/16
 * Time: 16:04
 */

namespace Pad\Repositories;
use Bican\Roles\Models\Role as Role;

class RoleRepository extends Repository
{

    static public function allRoles(){

        return Role::all();
    }
}