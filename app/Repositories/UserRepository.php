<?php 

namespace Pad\Repositories;

use Pad\User;
use Pad\Http\Requests;

Class UserRepository  extends Repository {

	protected static $msgErrors=[
						'name.required'=>"Debe especificar el Nombre del usuario",
						'username.required'=>"Debe especificar el username del User"];

	protected static $rules=[
						'name' => 'required',
						'username' => 'required|unique:users,username,:id',
						'roles'=> 'required'
	];
	
		
	static public function all() {

		return User::get();
	}

	static public  function getById($id) {

		return User::where('id', $id)->first();
	}


	static public function create($input) {

		$validate = self::validate($input,
							false,
							self::$rules,
							self::$msgErrors);

      	if ($validate->passes()) {

			$user= new User($input);
			$user->save();
			$user->attachRole($input['roles']);
        }

        return $validate->errors();
    }


	static public function delete(User $object) {

		$object->delete();
	}


	static public function update($input,User $object) {
           
      $validate = self::validate($input,
							$object->id,
							self::$rules,
							self::$msgErrors);

      if ($validate->passes()) {

		  $object->detachAllRoles();
		  $object->attachRole($input['roles']);
		  $object->fill($input)->save();

	  }

      return $validate->errors();               

	}
}

?>