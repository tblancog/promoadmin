<?php

use Illuminate\Database\Seeder;
use Bican\Roles\Models\Role;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['name'=> 'Admin','slug'=> 'admin',]);
        Role::create(['name'=> 'Aprobador','slug'=> 'aprob',]);
        Role::create(['name'=> 'Rol1','slug'=> 'r1',]);
        Role::create(['name'=> 'Rol2','slug'=> 'r2',]);
        Role::create(['name'=> 'Rol3','slug'=> 'r3',]);


//        $user= new \Pad\User(['name'=> 'John Doe','username'=> 'johndoe',]);
//        $user->roles()->save(Role::where('slug', 'admin')->first());

        factory(Pad\User::class, 10)->create()->each(function($u) {
            $u->roles()->save(
                            Role::find(random_int(1, 5))
            );
        });
    }
}
