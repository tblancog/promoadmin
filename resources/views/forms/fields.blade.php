<div class="form-group">
    {{ Form::text('name', null, ['placeholder'=> 'Nombre', 'class'=> 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::text('username', null, ['placeholder'=> 'Usuario', 'class'=> 'form-control']) }}
</div>
<div class="form-group">
{{--    {{ dump($selected_roles) }}--}}
    {{  Form::select('roles[]', $roles,
                                $selected_roles,
                     ['id'=> 'select2-roles', 'class'=> 'form-control', 'multiple'])  }}

@section('footer')
    <script>

       $("#select2-roles").select2(
               { placeholder: 'Seleccione Roles' }
       );

    </script>
@endsection
