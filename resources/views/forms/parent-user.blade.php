<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">

            <h2>{{ $title }}</h2>

            @include('partials.flash_msg')

            @include('forms.user')

        </div>
    </div>
</div>