
{!! Form::model($model, ['route'=> ['user.update', $model->id], 'method'=> $method, 'novalidate'=> 'novalidate']) !!}
<div class="row">
    @include('forms.fields')
</div>
<div class="row">
    <div class="form-group">
        {{ Form::submit('Guardar', ['class'=> 'btn btn-primary'] ) }}
        {{ link_to_route('user.index', 'Atrás', [], ['class'=> 'btn btn-default']) }}
    </div>
</div>
{!! Form::close() !!}