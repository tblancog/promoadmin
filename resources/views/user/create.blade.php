@extends('layouts.app')

@section('content')

    @include('forms.parent-user', ['title'=> 'Añadir Usuario',
                                   'model'=> new \Pad\User(),
                                   'selected_roles'=> [],
                                   'route'=> 'user.store',
                                   'method'=> 'POST'])

@stop