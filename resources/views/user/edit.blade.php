@extends('layouts.app')

@section('content')

    @include('forms.parent-user', ['title'=> 'Editar Usuario',
                                   'model'=> $user,
                                   'selected_roles'=> $user->roles->pluck('id')->toArray(),
                                   'route'=> 'user.update',
                                   'method'=> 'PATCH'])

@stop