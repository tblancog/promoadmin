@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h2>Usuarios</h2>
            <a class="btn btn-primary btn-sm" href="{{ route('user.create') }}">
                <i class="fa fa-plus">&nbsp;</i> Añadir Nuevo
            </a>
        </div>
        <div class="row">
            @if(count($users))
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Usuario</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ link_to('users/'.$user->id, $user->name) }}</td>
                            <td>{{ $user->username }}</td>
                            <td><a class="btn btn-sm btn-default" href="{{ route('user.edit', ['id'=> $user->id]) }}"><i class="fa fa-pencil-square-o fa-lg"></i>&nbsp;&nbsp;Editar</a>
                                <a class="btn btn-sm btn-danger" href="{{ route('user.destroy', ['id'=> $user->id]) }}"><i class="fa fa-trash-o fa-lg"></i>&nbsp;&nbsp;Eliminar</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            @else
                <h4 class="text-center">No existen registros</h4>
            @endif


        </div>
    </div>
@endsection

@section('footer')
@endsection